# search-for-new.sh -- help to search for new packages to forensics-extra
#
# This file is part of the forensics-extra.
#
# Copyright 2019 Joao Eriberto Mota Filho <eriberto@debian.org>
#
# You can use this program under the BSD-3-Clause conditions.

####################
### Main program ###
####################

### Check for excluded-packages
[ -e excluded-packages ] || { echo -e "\nI can't find excluded-packages file. Aborting.\n"; exit 1; }

### Check for the right path and load variables
[ -e variables ] || { echo -e "\nI can't find variables file. Aborting.\n"; exit 1; }
source variables

### Is there -f option?
[ -d tmp-search -a "$1" = "-f" ] && rm -rf tmp-search

### Is there a tmp-detect/ directory?
[ -d tmp-search ] \
    && { echo "I found tmp-search/ directory. Please, remove it or use -f option."; exit 1; } \
    || mkdir tmp-search

### Download the last 'list-of-packages' from forensics-all package. Source: salsa.
cd tmp-search
wget https://salsa.debian.org/pkg-security-team/forensics-all/raw/debian/master/list-of-packages
cd ..

### Unify lists
cat list-of-packages-extra tmp-search/list-of-packages | egrep '^[a-z0-9]' | \
    cut -d" " -f1 | sort -u > tmp-search/tmp-current-list

echo $WORDS1 $WORDS2 $WORDS3 $WORDS4 $WORDS5 | tr -s " " > tmp-search/tmp-words

echo -e "\n\nFiltering...\n"

### Initial search for packages
> tmp-search/tmp-apt
for i in $(cat tmp-search/tmp-words)
do
    LANG=C apt-cache search $i >> tmp-search/tmp-apt
done

# filtering by exclusions from variables file
cat tmp-search/tmp-apt | sort -u | egrep -v "($EXCLUSIONS)"> tmp-search/tmp-apt2

# filtering by existent packages
cp tmp-search/tmp-apt2 tmp-search/tmp-filter1
for i in $(cat tmp-search/tmp-current-list)
do
    cat tmp-search/tmp-filter1 | egrep -v "^$i -" > tmp-search/tmp-filter2
    cat tmp-search/tmp-filter2 > tmp-search/tmp-filter1
done

# filtering by exclusions from excluded-packages file
for i in $(cat excluded-packages | egrep '^[a-z0-9]')
do
    cat tmp-search/tmp-filter1 | egrep -v "^$i -" > tmp-search/tmp-filter2
    cat tmp-search/tmp-filter2 > tmp-search/tmp-filter1
done


cp tmp-search/tmp-filter1 tmp-search/final-list-possible-packages

# Final
echo -e "\n\nDone. See the results in tmp-search/final-list-possible-packages file.\n"
